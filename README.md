# darksoft-to-neosd

Convert ROMS from the Darksoft NeoGeo Multi format to the NeoSD format

## Usage
You need to have python3 available on your system

Then, download this repository and copy the "order.txt" file from the NeoBuilderUI directory next to the `convert.py` file.

Finally, open `cmd.exe`, navigate to your directory and execute the following command (by replacing the paths to your needs obviously).
```
python convert.py -x -w "C:\Path\To\Darksoft NeoGeo Roll-Up\games" "C:\Output\Dir\NeoSD"
```

You can remove the `-x` to remove the hacks and `-w` to remove the homebrews.

Then copy the data from the result directory into a micro SD (you need 7.2GB of data) and happy playing.

## NeoValidator Results
The result is almost as perfect as the one converted from the romset. Exceptions are:
* The homebrews are not supported by the NeoBuilder
* KOF 98, Fatal Fury 2, Sidekicks and Metal Slug X are patched to deactivate the copy protections in the darksoft build

![NeoValidator](neovalidator.png)

## Credits, Thanks and Licence
The script was written by [WydD](https://twitter.com/WydD).

You can find the complete Neo Geo Roll-Up Pack [here](https://www.arcade-projects.com/forums/index.php?thread/5059-neo-geo-roll-up-pack-all-roms-for-the-mvs-aes-multis/)

Thanks to SmokeMonster, Darksoft and the others who have contributed in creating this pack.

This script is published under [MIT licence](LICENCE).