# Result in the NeoValidator:
# Invalid CRC for KOF98, Fatal Fury 2, Sidekicks and Metal Slug X because the original pack uses patches
import math
from collections import namedtuple
import os
import struct

RomSet = namedtuple("RomSet", ["rom", "name", "year", "creator", "genre", "parent"])

Other = 0
Action = 1
BeatEmUp = 2
Sports = 3
Driving = 4
Platformer = 5
Mahjong = 6
Shooter = 7
Quiz = 8
Fighting = 9
Puzzle = 10

# extracted from the NeoBuilder.exe
data = [
    # grep 'new RomSet(' extract.txt  | sed -E 's/^\s+new //g' | sed 's/, new RomEntry../),/g' | sed 's/RomSet.Genre.//g' | sed 's/, null/, None/g' | sort
    RomSet("2020bb", "2020 Super Baseball (set 1)", 1991, "SNK / Pallas", Sports, None),
    RomSet("2020bba", "2020 Super Baseball (set 2)", 1991, "SNK / Pallas", Sports, "2020bb"),
    RomSet("2020bbh", "2020 Super Baseball (set 3)", 1991, "SNK / Pallas", Sports, "2020bb"),
    RomSet("3countb", "3 Count Bout", 1993, "SNK", Fighting, None),
    RomSet("alpham2", "Alpha Mission II", 1991, "SNK", Shooter, None),
    RomSet("alpham2p", "Alpha Mission II (prototype)", 1991, "SNK", Shooter, "alpham2"),
    RomSet("androdun", "Andro Dunos", 1992, "Visco", Shooter, None),
    RomSet("aodk", "Aggressors of Dark Kombat", 1994, "ADK / SNK", Fighting, None),
    RomSet("aof", "Art of Fighting", 1992, "SNK", Fighting, None),
    RomSet("aof2", "Art of Fighting 2", 1994, "SNK", Fighting, None),
    RomSet("aof2a", "Art of Fighting 2 (AES)", 1994, "SNK", Fighting, "aof2"),
    RomSet("aof3", "Art of Fighting 3", 1996, "SNK", Fighting, None),
    RomSet("aof3k", "Art of Fighting 3 (Korean)", 1996, "SNK", Fighting, "aof3"),
    RomSet("b2b", "Bang Bang Busters", 2000, "Visco", Action, None),
    RomSet("bakatono", "Bakatonosama Mahjong Manyuuki", 1991, "Monolith Corp.", Mahjong, None),
    RomSet("bangbead", "Bang Bead", 2000, "Visco", Sports, None),
    RomSet("bjourney", "Blue's Journey", 1990, "Alpha Denshi Co.", Platformer, None),
    RomSet("blazstar", "Blazing Star", 1998, "Yumekobo", Shooter, None),
    RomSet("breakers", "Breakers", 1996, "Visco", Fighting, None),
    RomSet("breakrev", "Breakers Revenge", 1998, "Visco", Fighting, None),
    RomSet("bstars", "Baseball Stars Professional", 1990, "SNK", Sports, None),
    RomSet("bstars2", "Baseball Stars 2", 1992, "SNK", Sports, None),
    RomSet("bstarsh", "Baseball Stars Professional (AES", 1990, "SNK", Sports, "bstars"),
    RomSet("burningf", "Burning Fight", 1991, "SNK", BeatEmUp, None),
    RomSet("burningfh", "Burning Fight (AES)", 1991, "SNK", BeatEmUp, "burningf"),
    RomSet("burningfp", "Burning Fight (prototype)", 1991, "SNK", BeatEmUp, "burningf"),
    RomSet("burningfpa", "Burning Fight (prototype 2)", 1991, "SNK", BeatEmUp, "burningf"),
    RomSet("crswd2bl", "Crossed Swords 2", 1996, "SNK/ADK", Action, None),
    RomSet("crsword", "Crossed Swords", 1991, "Alpha Denshi Co.", Action, None),
    RomSet("ct2k3sa", "Crouching Tiger Hidden Dragon al", 2003, "bootleg", Fighting, "kof2001"),
    RomSet("ct2k3sp", "Crouching Tiger Hidden Dragon SP", 2003, "bootleg", Fighting, "kof2001"),
    RomSet("cthd2003", "Crouching Tiger Hidden Dragon", 2003, "bootleg", Fighting, "kof2001"),
    RomSet("ctomaday", "Captain Tomaday", 1999, "Visco", Shooter, None),
    RomSet("cyberlip", "Cyber-Lip", 1990, "SNK", Action, None),
    RomSet("diggerma", "Digger Man (prototype)", 2000, "Kyle Hodgetts", Puzzle, None),
    RomSet("doubledr", "Double Dragon", 1995, "Technos Japan", Fighting, None),
    RomSet("dragonsh", "Dragon's Heaven (dev)", 2000, "Face", Fighting, None),
    RomSet("eightman", "Eight Man", 1991, "SNK / Pallas", Action, None),
    RomSet("fatfursp", "Fatal Fury Special", 1993, "SNK", Fighting, None),
    RomSet("fatfurspa", "Fatal Fury Special (AES)", 1993, "SNK", Fighting, "fatfursp"),
    RomSet("fatfury1", "Fatal Fury - King of Fighters", 1991, "SNK", Fighting, None),
    RomSet("fatfury2", "Fatal Fury 2", 1992, "SNK", Fighting, None),
    RomSet("fatfury3", "Fatal Fury 3", 1995, "SNK", Fighting, None),
    RomSet("fbfrenzy", "Football Frenzy", 1992, "SNK", Sports, None),
    RomSet("fightfev", "Fight Fever (set 1)", 1994, "Viccom", Fighting, None),
    RomSet("fightfeva", "Fight Fever (set 2)", 1994, "Viccom", Fighting, "fightfev"),
    RomSet("flipshot", "Battle Flip Shot", 1998, "Visco", Sports, None),
    RomSet("froman2b", "Idol Mahjong Final Romance 2", 1995, "Video System", Mahjong, None),
    RomSet("fswords", "Samurai Shodown III (Korean)", 1995, "SNK", Fighting, "samsho3"),
    RomSet("galaxyfg", "Galaxy Fight", 1995, "Sunsoft", Fighting, None),
    RomSet("ganryu", "Ganryu", 1999, "Visco", Action, None),
    RomSet("garou", "Garou: Mark of the Wolves", 1999, "SNK", Fighting, None),
    RomSet("garoubl", "Garou: Mark of the Wolves (boot)", 1999, "bootleg", Fighting, "garou"),
    RomSet("garouh", "Garou: Mark of the Wolves (AES)", 1999, "SNK", Fighting, "garou"),
    RomSet("garoup", "Garou: Mark of the Wolves proto", 1999, "SNK", Fighting, "garou"),
    RomSet("ghostlop", "Ghostlop (prototype)", 1996, "Data East", Puzzle, None),
    RomSet("goalx3", "Goal! Goal! Goal!", 1995, "Visco", Sports, None),
    RomSet("gowcaizr", "Voltage Fighter - Gowcaizer", 1995, "Technos Japan", Fighting, None),
    RomSet("gpilots", "Ghost Pilots", 1991, "SNK", Shooter, None),
    RomSet("gpilotsh", "Ghost Pilots (AES)", 1991, "SNK", Shooter, "gpilots"),
    RomSet("gururin", "Gururin", 1994, "Face", Puzzle, None),
    RomSet("ironclad", "Iron Clad (prototype)", 1996, "Saurus", Shooter, None),
    RomSet("ironclado", "Iron Clad (prototype. bootleg)", 1996, "bootleg", Shooter, "ironclad"),
    RomSet("irrmaze", "The Irritating Maze", 1997, "SNK / Saurus", Other, None),
    RomSet("janshin", "Janshin Densetsu", 1994, "Aicom", Mahjong, None),
    RomSet("jockeygp", "Jockey Grand Prix (set 1)", 2001, "Sun Amusement", Sports, None),
    RomSet("jockeygpa", "Jockey Grand Prix (set 2)", 2001, "Sun Amusement", Sports, "jockeygp"),
    RomSet("joyjoy", "Joy Joy Kid", 1990, "SNK", Puzzle, None),
    RomSet("kabukikl", "Kabuki Klash", 1995, "Hudson", Fighting, None),
    RomSet("karnovr", "Karnov's Revenge", 1994, "Data East", Fighting, None),
    RomSet("kf10thep", "The King of Fighters 10th EP", 2005, "bootleg", Fighting, None),
    RomSet("kf2k2mp", "The King of Fighters 2002 MP", 2002, "bootleg", Fighting, "kof2002"),
    RomSet("kf2k2mp2", "The King of Fighters 2002 MP2", 2002, "bootleg", Fighting, "kof2002"),
    RomSet("kf2k2pla", "The King of Fighters 2002 Plus2", 2002, "bootleg", Fighting, "kof2002"),
    RomSet("kf2k2pls", "The King of Fighters 2002 Plus", 2002, "bootleg", Fighting, "kof2002"),
    RomSet("kf2k3bl", "The King of Fighters 2003 (boot)", 2003, "bootleg", Fighting, "kof2003"),
    RomSet("kf2k3bla", "The King of Fighters 2003 (boot2", 2003, "bootleg", Fighting, "kof2003"),
    RomSet("kf2k3pl", "The King of Fighters 2004 Plus", 2003, "bootleg", Fighting, "kof2003"),
    RomSet("kf2k3upl", "The King of Fighters 2004 UP", 2003, "bootleg", Fighting, "kof2003"),
    RomSet("kf2k5uni", "The King of Fighters 10th Unique", 2004, "bootleg", Fighting, None),
    RomSet("kizuna", "Kizuna Encounter", 1996, "SNK", Fighting, None),
    RomSet("kof2000", "The King of Fighters 2000", 2000, "SNK", Fighting, None),
    RomSet("kof2000n", "The King of Fighters 2k (unenc)", 2000, "SNK", Fighting, "kof2000"),
    RomSet("kof2001", "The King of Fighters 2001", 2001, "Eolith / SNK", Fighting, None),
    RomSet("kof2001h", "The King of Fighters 2001 (AES)", 2001, "Eolith / SNK", Fighting, "kof2001"),
    RomSet("kof2002", "The King of Fighters 2002", 2002, "Eolith/Playmore", Fighting, None),
    RomSet("kof2002b", "The King of Fighters 2002 (boot)", 2002, "bootleg", Fighting, "kof2002"),
    RomSet("kof2003", "The King of Fighters 2003", 2003, "SNK Playmore", Fighting, None),
    RomSet("kof2003h", "The King of Fighters 2003 (AES)", 2003, "SNK Playmore", Fighting, "kof2003"),
    RomSet("kof2k4se", "The King of Fighters SE 2004", 2004, "bootleg", Fighting, "kof2002"),
    RomSet("kof94", "The King of Fighters '94", 1994, "SNK", Fighting, None),
    RomSet("kof95", "The King of Fighters '95", 1995, "SNK", Fighting, None),
    RomSet("kof95a", "The King of Fighters '95 (Alt)", 1995, "SNK", Fighting, "kof95"),
    RomSet("kof95h", "The King of Fighters '95 (AES)", 1995, "SNK", Fighting, "kof95"),
    RomSet("kof96", "The King of Fighters '96", 1996, "SNK", Fighting, None),
    RomSet("kof96h", "The King of Fighters '96 (AES)", 1996, "SNK", Fighting, "kof96"),
    RomSet("kof97", "The King of Fighters '97", 1997, "SNK", Fighting, None),
    RomSet("kof97h", "The King of Fighters '97 (AES)", 1997, "SNK", Fighting, "kof97"),
    RomSet("kof97k", "The King of Fighters '97 (Kor)", 1997, "SNK", Fighting, "kof97"),
    RomSet("kof97oro", "The King of Fighters '97 Oroshi", 1997, "bootleg", Fighting, "kof97"),
    RomSet("kof97pls", "The King of Fighters '97 Plus", 1997, "bootleg", Fighting, "kof97"),
    RomSet("kof98", "The King of Fighters '98", 1998, "SNK", Fighting, None),
    RomSet("kof98a", "The King of Fighters '98 (Set 2)", 1998, "SNK", Fighting, "kof98"),
    RomSet("kof98h", "The King of Fighters '98 (AES)", 1998, "SNK", Fighting, "kof98"),
    RomSet("kof98k", "The King of Fighters '98 (Kor)", 1998, "SNK", Fighting, "kof98"),
    RomSet("kof98ka", "The King of Fighters '98 (Kor2)", 1998, "SNK", Fighting, "kof98"),
    RomSet("kof99", "The King of Fighters '99", 1999, "SNK", Fighting, None),
    RomSet("kof99e", "The King of Fighters '99 (prev)", 1999, "SNK", Fighting, "kof99"),
    RomSet("kof99h", "The King of Fighters '99 (AES)", 1999, "SNK", Fighting, "kof99"),
    RomSet("kof99k", "The King of Fighters '99 (Kor,A)", 1999, "SNK", Fighting, "kof99"),
    RomSet("kof99ka", "The King of Fighters '99 (Kor)", 1999, "SNK", Fighting, "kof99"),
    RomSet("kof99p", "The King of Fighters '99 (proto)", 1999, "SNK", Fighting, "kof99"),
    RomSet("kog", "King of Gladiator", 1997, "bootleg", Fighting, "kof97"),
    RomSet("kotm", "King of the Monsters (set 1)", 1991, "SNK", Fighting, None),
    RomSet("kotm2", "King of the Monsters 2", 1992, "SNK", Fighting, None),
    RomSet("kotm2a", "King of the Monsters 2 (set 2)", 1992, "SNK", Fighting, "kotm2"),
    RomSet("kotm2p", "King of the Monsters 2 (proto)", 1992, "SNK", Fighting, "kotm2"),
    RomSet("kotmh", "King of the Monsters (set 2)", 1991, "SNK", Fighting, "kotm"),
    RomSet("lans2004", "Lansquenet 2004", 1998, "bootleg", Action, "shocktr2"),
    RomSet("lastblad", "The Last Blade", 1997, "SNK", Fighting, None),
    RomSet("lastbladh", "The Last Blade (AES)", 1997, "SNK", Fighting, "lastblad"),
    RomSet("lastbld2", "The Last Blade 2", 1998, "SNK", Fighting, None),
    RomSet("lastsold", "The Last Blade (Korean)", 1997, "SNK", Fighting, "lastblad"),
    RomSet("lbowling", "League Bowling", 1990, "SNK", Sports, None),
    RomSet("legendos", "Legend of Success Joe", 1991, "SNK", BeatEmUp, None),
    RomSet("lresort", "Last Resort", 1992, "SNK", Shooter, None),
    RomSet("lresortp", "Last Resort (prototype)", 1992, "SNK", Shooter, "lresort"),
    RomSet("magdrop2", "Magical Drop II", 1996, "Data East", Puzzle, None),
    RomSet("magdrop3", "Magical Drop III", 1997, "Data East", Puzzle, None),
    RomSet("maglord", "Magician Lord", 1990, "Alpha Denshi Co.", Platformer, None),
    RomSet("maglordh", "Magician Lord (AES)", 1990, "Alpha Denshi Co.", Platformer, "maglord"),
    RomSet("mahretsu", "Mahjong Kyo Retsuden", 1990, "SNK", Mahjong, None),
    RomSet("marukodq", "Chibi Marukochan Deluxe Quiz", 1995, "Takara", Quiz, None),
    RomSet("matrim", "Matrimelee", 2002, "Noise Factory", Fighting, None),
    RomSet("matrimbl", "Matrimelee (bootleg)", 2002, "bootleg", Fighting, "matrim"),
    RomSet("miexchng", "Money Idol Exchanger", 1997, "Face", Puzzle, None),
    RomSet("minasan", "Minasan no Okagesamadesu!", 1990, "Monolith Corp.", Other, None),
    RomSet("moshougi", "Master of Shougi", 1995, "ADK / SNK", Other, None),
    RomSet("ms4plus", "Metal Slug 4 Plus (bootleg)", 2002, "bootleg", Action, "mslug4"),
    RomSet("ms5pcb", "Metal Slug 5 (PCB)", 2003, "SNK Playmore", Action, None),
    RomSet("ms5plus", "Metal Slug 5 Plus", 2003, "bootleg", Action, "mslug5"),
    RomSet("mslug", "Metal Slug - Super Vehicle-001", 1996, "Nazca", Action, None),
    RomSet("mslug2", "Metal Slug 2", 1998, "SNK", Action, None),
    RomSet("mslug2t", "Metal Slug 2 Turbo", 1998, "SNK", Action, "mslug2"),
    RomSet("mslug3", "Metal Slug 3", 2000, "SNK", Action, None),
    RomSet("mslug3b6", "Metal Slug 6 (Metal Slug 3 boot)", 2000, "bootleg", Action, "mslug3"),
    RomSet("mslug3h", "Metal Slug 3 (AES)", 2000, "SNK", Action, "mslug3"),
    RomSet("mslug4", "Metal Slug 4", 2002, "Mega / Playmore", Action, None),
    RomSet("mslug4h", "Metal Slug 4 (AES)", 2002, "Mega / Playmore", Action, "mslug4"),
    RomSet("mslug5", "Metal Slug 5", 2003, "SNK Playmore", Action, None),
    RomSet("mslug5h", "Metal Slug 5 (AES)", 2003, "SNK Playmore", Action, "mslug5"),
    RomSet("mslugx", "Metal Slug X", 1999, "SNK", Action, None),
    RomSet("mutnat", "Mutation Nation", 1992, "SNK", BeatEmUp, None),
    RomSet("nam1975", "NAM-1975", 1990, "SNK", Action, None),
    RomSet("ncombat", "Ninja Combat", 1990, "Alpha Denshi Co.", BeatEmUp, None),
    RomSet("ncombath", "Ninja Combat (AES)", 1990, "Alpha Denshi Co.", BeatEmUp, "ncombat"),
    RomSet("ncommand", "Ninja Commando", 1992, "Alpha Denshi Co.", Shooter, None),
    RomSet("neobombe", "Neo Bomberman", 1997, "Hudson", Puzzle, None),
    RomSet("neocup98", "Neo-Geo Cup '98", 1998, "SNK", Sports, None),
    RomSet("neodrift", "Neo Drift Out", 1996, "Visco", Driving, None),
    RomSet("neomrdo", "Neo Mr. Do!", 1996, "Visco", Action, None),
    RomSet("ninjamas", "Ninja Master's", 1996, "ADK / SNK", Fighting, None),
    RomSet("nitd", "Nightmare in the Dark", 2000, "Eleven/Gavaking", Action, None),
    RomSet("nitdbl", "Nightmare in the Dark (bootleg)", 2001, "bootleg", Action, "nitd"),
    RomSet("overtop", "Over Top", 1996, "ADK", Driving, None),
    RomSet("panicbom", "Panic Bomber", 1994, "Eighting/Hudson", Puzzle, None),
    RomSet("pbobbl2n", "Puzzle Bobble 2", 1999, "Taito", Puzzle, None),
    RomSet("pbobblen", "Puzzle Bobble", 1994, "Taito", Puzzle, None),
    RomSet("pbobblenb", "Puzzle Bobble (bootleg)", 1994, "bootleg", Puzzle, "pbobblen"),
    RomSet("pgoal", "Pleasure Goal", 1996, "Saurus", Sports, None),
    RomSet("pnyaa", "Pochi and Nyaa", 2003, "Aiky / Taito", Puzzle, None),
    RomSet("popbounc", "Pop 'n Bounce / Gapporin", 1997, "Video System Co.", Puzzle, None),
    RomSet("preisle2", "Prehistoric Isle 2", 1999, "Yumekobo", Shooter, None),
    RomSet("pspikes2", "Power Spikes II", 1994, "Video System Co.", Sports, None),
    RomSet("pulstar", "Pulstar", 1995, "Aicom", Shooter, None),
    RomSet("puzzldpr", "Puzzle De Pon! R!", 1997, "Taito", Puzzle, "puzzledp"),
    RomSet("puzzledp", "Puzzle De Pon!", 1995, "Taito", Puzzle, None),
    RomSet("quizdai2", "Quiz Daisousa Sen part 2", 1992, "SNK", Quiz, None),
    RomSet("quizdais", "Quiz Daisousa Sen", 1991, "SNK", Quiz, None),
    RomSet("quizdaisk", "Quiz Daisousa Sen (Korean)", 1991, "SNK", Quiz, "quizdais"),
    RomSet("quizkof", "Quiz King of Fighters", 1995, "Saurus", Quiz, None),
    RomSet("quizkofk", "Quiz King of Fighters (Korean)", 1995, "Saurus", Quiz, "quizkof"),
    RomSet("ragnagrd", "Ragnagard", 1996, "Saurus", Fighting, None),
    RomSet("rbff1", "Real Bout Fatal Fury", 1995, "SNK", Fighting, None),
    RomSet("rbff1a", "Real Bout Fatal Fury (bugfix)", 1995, "SNK", Fighting, "rbff1"),
    RomSet("rbff1k", "Real Bout Fatal Fury (Korean)", 1995, "SNK", Fighting, "rbff1"),
    RomSet("rbff2", "Real Bout Fatal Fury 2", 1998, "SNK", Fighting, None),
    RomSet("rbff2h", "Real Bout Fatal Fury 2 (AES)", 1998, "SNK", Fighting, "rbff2"),
    RomSet("rbff2k", "Real Bout Fatal Fury 2 (Korean)", 1998, "SNK", Fighting, "rbff2"),
    RomSet("rbffspec", "Real Bout Fatal Fury Special", 1996, "SNK", Fighting, None),
    RomSet("rbffspeck", "Real Bout Fatal Fury Special (K)", 1996, "SNK", Fighting, "rbffspec"),
    RomSet("ridhero", "Riding Hero", 1990, "SNK", Driving, None),
    RomSet("ridheroh", "Riding Hero (set 2)", 1990, "SNK", Driving, "ridhero"),
    RomSet("roboarma", "Robo Army (Alt)", 1991, "SNK", BeatEmUp, "roboarmy"),
    RomSet("roboarmy", "Robo Army", 1991, "SNK", BeatEmUp, None),
    RomSet("rotd", "Rage of the Dragons", 2002, "Evoga / Playmore", Fighting, None),
    RomSet("rotdh", "Rage of the Dragons (AES)", 2002, "Evoga / Playmore", Fighting, "rotd"),
    RomSet("s1945p", "Strikers 1945 Plus", 1999, "Psikyo", Shooter, None),
    RomSet("samsh5sp", "Samurai Shodown V Special", 2004, "Yuki Enterprise", Fighting, None),
    RomSet("samsh5sph", "Samurai Shodown V Special (AES)", 2004, "Yuki Enterprise", Fighting, "samsh5sp"),
    RomSet("samsh5spho", "Samurai Shodown V Sp (AES Ori)", 2004, "Yuki Enterprise", Fighting, "samsh5sp"),
    RomSet("samsho", "Samurai Shodown", 1993, "SNK", Fighting, None),
    RomSet("samsho2", "Samurai Shodown II", 1994, "SNK", Fighting, None),
    RomSet("samsho2k", "Samurai Shodown II (Korean)", 1994, "SNK", Fighting, "samsho2"),
    RomSet("samsho2ka", "Samurai Shodown II (Korean,Alt)", 1994, "SNK", Fighting, "samsho2"),
    RomSet("samsho3", "Samurai Shodown III", 1995, "SNK", Fighting, None),
    RomSet("samsho3h", "Samurai Shodown III (AES)", 1995, "SNK", Fighting, "samsho3"),
    RomSet("samsho4", "Samurai Shodown IV", 1996, "SNK", Fighting, None),
    RomSet("samsho4k", "Samurai Shodown IV (Korean)", 1996, "SNK", Fighting, "samsho4"),
    RomSet("samsho5", "Samurai Shodown V", 2003, "Yuki Enterprise", Fighting, None),
    RomSet("samsho5b", "Samurai Shodown V (bootleg)", 2003, "bootleg", Fighting, "samsho5"),
    RomSet("samsho5h", "Samurai Shodown V (AES)", 2003, "Yuki Enterprise", Fighting, "samsho5"),
    RomSet("samshoh", "Samurai Shodown (AES)", 1993, "SNK", Fighting, "samsho"),
    RomSet("savagere", "Savage Reign", 1995, "SNK", Fighting, None),
    RomSet("sbp", "Super Bubble Pop", 2004, "Vektorlogic", Puzzle, None),
    RomSet("sdodgeb", "Super Dodge Ball", 1996, "Technos Japan", Sports, None),
    RomSet("sengoku", "Sengoku", 1991, "SNK", BeatEmUp, None),
    RomSet("sengoku2", "Sengoku 2", 1993, "SNK", BeatEmUp, None),
    RomSet("sengoku3", "Sengoku 3", 2001, "Noise ", BeatEmUp, None),
    RomSet("sengokuh", "Sengoku (AES)", 1991, "SNK", BeatEmUp, "sengoku"),
    RomSet("shocktr2", "Shock Troopers 2", 1998, "Saurus", Action, None),
    RomSet("shocktro", "Shock Troopers (set 1)", 1997, "Saurus", Action, None),
    RomSet("shocktroa", "Shock Troopers (set 2)", 1997, "Saurus", Action, "shocktro"),
    RomSet("socbrawl", "Soccer Brawl", 1991, "SNK", Sports, None),
    RomSet("socbrawlh", "Soccer Brawl (AES)", 1991, "SNK", Sports, "socbrawl"),
    RomSet("sonicwi2", "Aero Fighters 2", 1994, "Video System Co.", Shooter, None),
    RomSet("sonicwi3", "Aero Fighters 3", 1995, "Video System Co.", Shooter, None),
    RomSet("spinmast", "Spin Master", 1993, "Data East", Platformer, None),
    RomSet("ssideki", "Super Sidekicks", 1992, "SNK", Sports, None),
    RomSet("ssideki2", "Super Sidekicks 2", 1994, "SNK", Sports, None),
    RomSet("ssideki3", "Super Sidekicks 3", 1995, "SNK", Sports, None),
    RomSet("ssideki4", "Super Sidekicks 4", 1996, "SNK", Sports, None),
    RomSet("stakwin", "Stakes Winner", 1995, "Saurus", Sports, None),
    RomSet("stakwin2", "Stakes Winner 2", 1996, "Saurus", Sports, None),
    RomSet("strhoop", "Street Hoop", 1994, "Data East", Sports, None),
    RomSet("superspy", "The Super Spy", 1990, "SNK", BeatEmUp, None),
    RomSet("svc", "SNK vs. Capcom", 2003, "SNK Playmore", Fighting, None),
    RomSet("svcboot", "SNK vs. Capcom (bootleg)", 2003, "bootleg", Fighting, "svc"),
    RomSet("svcpcb", "SNK vs. Capcom (PCB)", 2003, "SNK Playmore", Fighting, None),
    RomSet("svcpcba", "SNK vs. Capcom (PCB,alt)", 2003, "SNK Playmore", Fighting, "svcpcb"),
    RomSet("svcplus", "SNK vs. Capcom Plus", 2003, "bootleg", Fighting, "svc"),
    RomSet("svcplusa", "SNK vs. Capcom Plus 2", 2003, "bootleg", Fighting, "svc"),
    RomSet("svcsplus", "SNK vs. Capcom Super Plus", 2003, "bootleg", Fighting, "svc"),
    RomSet("tophuntr", "Top Hunter", 1994, "SNK", Platformer, None),
    RomSet("tophuntrh", "Top Hunter (AES)", 1994, "SNK", Platformer, "tophuntr"),
    RomSet("tpgolf", "Top Player's Golf", 1990, "SNK", Sports, None),
    RomSet("trally", "Thrash Rally", 1991, "Alpha Denshi Co.", Driving, None),
    RomSet("turfmast", "Neo Turf Masters", 1996, "Nazca", Sports, None),
    RomSet("twinspri", "Twinkle Star Sprites", 1996, "ADK / SNK", Puzzle, None),
    RomSet("tws96", "Tecmo World Soccer '96", 1996, "Tecmo", Sports, None),
    RomSet("viewpoin", "Viewpoint", 1992, "Sammy / Aicom", Shooter, None),
    RomSet("vliner", "V-Liner (set 1)", 2001, "Dyna/BrezzaSoft", Other, None),
    RomSet("vlinero", "V-Liner (set 2)", 2001, "Dyna/BrezzaSoft", Other, "vliner"),
    RomSet("wakuwak7", "Waku Waku 7", 1996, "Sunsoft", Fighting, None),
    RomSet("wh1", "World Heroes", 1992, "Alpha Denshi Co.", Fighting, None),
    RomSet("wh1h", "World Heroes (AES)", 1992, "Alpha Denshi Co.", Fighting, "wh1"),
    RomSet("wh1ha", "World Heroes (set 3)", 1992, "Alpha Denshi Co.", Fighting, "wh1"),
    RomSet("wh2", "World Heroes 2", 1993, "ADK", Fighting, None),
    RomSet("wh2j", "World Heroes 2 Jet", 1994, "ADK / SNK", Fighting, None),
    RomSet("whp", "World Heroes Perfect", 1995, "ADK / SNK", Fighting, None),
    RomSet("wjammers", "Windjammers", 1994, "Data East", Sports, None),
    RomSet("zedblade", "Zed Blade", 1994, "NMK", Shooter, None),
    RomSet("zintrckb", "Zintrick", 1996, "SNK/ADK", Puzzle, None),
    RomSet("zupapa", "Zupapa!", 2001, "SNK", Action, None),
]
v2sizes = {
    # grep -E "ROM_START|ymsnd.deltat" neogeo.cpp  | awk '/ROM_START/ {a = $2} /ROM_REGION/ {print "\"" a "\": " $2}'
    "nam1975": 0x180000,
    "bstars": 0x080000,
    "bstarsh": 0x080000,
    "tpgolf": 0x200000,
    "mahretsu": 0x180000,
    "maglord": 0x100000,
    "maglordh": 0x100000,
    "ridhero": 0x200000,
    "ridheroh": 0x200000,
    "alpham2p": 0x080000,
    "ncombat": 0x080000,
    "ncombath": 0x080000,
    "cyberlip": 0x080000,
    "superspy": 0x080000,
    "burningfp": 0x080000,
    "lbowling": 0x080000,
    "gpilots": 0x080000,
    "gpilotsh": 0x080000,
    "gpilotsp": 0x080000,
    "joyjoy": 0x080000,
    "minasan": 0x100000,
}
# manual patches due to rom_copy
v2sizes["alpham2p"] += 0x180000
del v2sizes["burningfp"]
v2sizes["bjourney"] = 0x200000

homebrew = [
    RomSet("columnsn", "Columns", 2008, "Homebrew / Sivak Games", Puzzle, None),
    RomSet("frogfest", "Frog Feast", 2006, "Homebrew / Rastersoft", Other, None),
    RomSet("neofight", "Neo Fight", 2015, "Homebrew / Kannagi", Fighting, None),
    RomSet("neothund", "Neo Thunder", 2012, "Homebrew / Sebastian Mihai", Shooter, None),
    RomSet("tetrismn", "NeoGeo 2 Player Tetris", 2008, "Homebrew / Crim", Puzzle, None),
    RomSet("totc", "Treasure of the Caribbean", 2012, "Homebrew / Neo Conception International", Puzzle, None),
]

hacks = [
    RomSet("samsho5x", "Samurai Shodown V (xbox hack)", 2003, "bootleg", Fighting, None),
]

utils = [
    RomSet("monitor", "Monitor Test Tool", 2014, "NeoHomeBrew", Other, None)
]

aliases = {
    "brningfh": "burningfh",
    "brningfp": "burningfp",
    "brnngfpa": "burningfpa",
    "fghtfeva": "fightfeva",
    "ftfurspa": "fatfurspa",
    "irnclado": "ironclado",
    "lstbladh": "lastbladh",
    "pbbblenb": "pbobblenb",
    "quizdask": "quizdaisk",
    "rbffspck": "rbffspeck",
    "scbrawlh": "socbrawlh",
    "shcktroa": "shocktroa",
    "smsh5sph": "samsh5sph",
    "smsh5spo": "samsh5spho",
    "smsho2k2": "samsho2ka",
    "tphuntrh": "tophuntrh",
    "MonitorTest": "monitor"
}

screenshots = {
    entry.split(", ", 1)[0]: int(entry.split(", ", 1)[1])
    for entry in open("./order.txt").read().split("\n")
    if entry
}


def strip_bytes(content):
    original_len = len(content)
    content = bytearray(content)
    content = content.rstrip(b"\x00")
    len_content = len(content)
    actual_len = (math.ceil(len_content / 65536) * 65536)
    if len_content != actual_len:
        content.extend(b"\x00" * (actual_len - len_content))
    if original_len != actual_len:
        print("\tStrip content from 0x%x to 0x%x" % (original_len, actual_len))
    return content


filler1 = b'\x00' * (128 + 290 - 17 - 17)
filler2 = b'\x00' * (4096 - 512)


def generate_neo(result_dir, darksoft_dir, romset):
    with open(result_dir + romset.rom + ".neo", "wb") as res:
        print("Generating rom %s of name %s" % (romset.rom, romset.name))
        c = []
        p = []
        s = []
        m = []
        v = []
        v2_size = v2sizes.get(romset.rom, 0)
        for f in sorted(os.listdir(darksoft_dir)):
            if f == "fpga" or f == "old_prom":
                continue
            file = darksoft_dir + os.sep + f
            content = open(file, "rb").read()
            if f[0] == "c":
                target = c
                # The C section is not encoded the same way on
                # bytes like A B C D are actually A C B D
                content = bytearray(content)
                content[1::4], content[2::4] = content[2::4], content[1::4]
            elif f[0] == "m":
                target = m
            elif f[0] == "p":
                content = bytearray(content)
                target = p
            elif f[0] == "s":
                target = s
            elif f[0] == "v":
                target = v
                content = strip_bytes(content)
                if v2_size:
                    v1 = strip_bytes(content[:len(content) - v2_size])
                    if romset.rom == "maglordh":
                        v1 = v1[:0x80000]
                        print("\tException on %s, v1 rom is repeated 3 times" % romset.rom)
                    target.append(v1)
                    content = content[len(content) - v2_size:]
            else:
                raise Exception("I DONT KNOW THIS FILE " + f)
            target.append(content)
        c_size = sum([len(e) for e in c])
        p_size = sum([len(e) for e in p])
        s_size = sum([len(e) for e in s])
        m_size = sum([len(e) for e in m])
        v_size = sum([len(e) for e in v])
        res.write(struct.pack("3sB", b'NEO', 1))
        res.write(struct.pack("<IIIIII", p_size, s_size, m_size, v_size - v2_size, v2_size, c_size))
        # undocumented field
        ngh = p[0][264] | (p[0][265] << 8)
        res.write(struct.pack("<iIII", romset.year, romset.genre, screenshots.get(romset.rom, 0), ngh))
        res.write(struct.pack("33s", romset.name.encode("ascii")))
        res.write(struct.pack("17s", romset.creator.encode("ascii")))
        # undocumented again
        res.write(struct.pack("17s", b""))
        res.write(struct.pack("17s", romset.parent.encode("ascii") if romset.parent else b""))
        res.write(filler1)
        res.write(filler2)
        for section in [p, s, m, v, c]:
            for content in section:
                res.write(content)

    print("\tdone")


def convert(result_dir, darksoft_dir, include_homebrews, include_hacks):
    total_roms = data
    if include_homebrews:
        total_roms += homebrew
    if include_hacks:
        total_roms += hacks
    total_roms += utils
    romset_idx = {rom.rom: rom for rom in total_roms}
    for game in os.listdir(darksoft_dir):
        if game == "menu":
            continue
        romname = aliases.get(game, game)
        if romname not in romset_idx:
            print("Warning! Unselected item!", game)
            continue
        generate_neo(result_dir, darksoft_dir + game, romset_idx[romname])


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-x", "--hacks", help="include hacks", action="store_true")
    parser.add_argument("-w", "--homebrews", help="include homebrews", action="store_true")
    parser.add_argument("darksoft_games_dir", help="Points to the darksoft games/ directory")
    parser.add_argument("output", help="output directory")
    args = parser.parse_args()

    output_dir = os.path.normpath(args.output) + os.sep
    darksoft_dir = os.path.normpath(args.darksoft_games_dir) + os.sep

    if not os.path.exists(darksoft_dir+"menu"):
        print("The darksoft dir doesnt seem to point to darksoft/games, exiting now...")
        exit(1)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # execute only if run as a script
    convert(output_dir, darksoft_dir, args.homebrews, args.hacks)

